import React from "react";
import Button from "./Button";

class Banner extends React.Component {
	constructor(){
		super();
		//default the required state - should be extracted to schema
		this.state = {
			title: "",
			subtitle:"",
			button: "",
			url: "",
			background: ""
		};
	}
	componentWillMount() {
		this.initBannerState();
	}
	// make server effect
	getData() {
		return null;
	}

	initBannerState() {
		console.log(this.props);
		//maps props to state.
		if(this.props.data) {
			let bannerDetails = this.props.data;

			this.state = {
				title: bannerDetails.title,
				subtitle: bannerDetails.subtitle,
				button: bannerDetails.button,
				url: bannerDetails.url,
				background: bannerDetails.backgroundImage
			}	
		}
		else {

		}
	}

	render() {	
		return (

			<div className='banner'>
				<h1>{this.state.title}</h1>
				<h4>{this.state.subtitle}</h4>
				<Button title={this.state.button} url={this.state.url} />
			</div>
		);
	}
}
export default Banner;