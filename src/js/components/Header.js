import React from 'react';
import ReactDOM from 'react-dom';
import Nav from './Header/Nav';

import HEADERMOCKS from '../mocks/HEADER.json';

class Header extends React.Component {
	constructor() {
		super();
		this.state = {
			navItems: []
		};
	}

	componentWillMount() {
		// load nav items on mount for data
		let loadNavItems = this.getNavItems(); 
	  	if(HEADERMOCKS && loadNavItems == null) {
	  		this.handleNavItems(HEADERMOCKS);
	  		console.log(this.state);
	  	}

	  	else {
	  		this.handleNavItems(loadNavItems)
	  	}
	}
// simple setting the state for navItems Object with data
	handleNavItems(data) {
		this.state = { navItems : data };
	}
// function to 
// make server call default to null for mocks to take effect
	getNavItems() {
		return null;
	}

	handleCollaspeClick() {
		let head = ReactDOM.findDOMNode(this.refs.header),
		icon = ReactDOM.findDOMNode(this.refs.iconHolder);

		head.classList.toggle('collasped');
		icon.classList.toggle('close');
		// document.body.style.overflow = "hidden"
	}

	render() {	
		return (
			<header ref="header" className="header">
				<Nav navItemsList={this.state}/>
				<i className="collaspe-menu"
					ref="iconHolder"
					onClick={::this.handleCollaspeClick}
				></i>
			</header>
		);
	}
}
export default Header;