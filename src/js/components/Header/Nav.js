import React from 'react';
import NavItem from './NavItem';

class Nav extends React.Component {
	constructor() {
		super();
		this.state = {
			navItems: []
		};
	}
	componentWillMount() {
		this.state.navItems = this.props.navItemsList.navItems;
	}
	renderNavItems(itemValue, indexValue){
		return <NavItem title={itemValue.title} route={itemValue.route} key={indexValue} id={indexValue} />;
	}
	render() {
		return (
			<ul className='head-nav'>
				{this.state.navItems.map((item, i) => {
				    return this.renderNavItems(item, i);
				})}
			</ul>
		); 
	}
}
export default Nav;