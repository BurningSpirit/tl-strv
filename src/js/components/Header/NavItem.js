import React from "react";
import { Link } from "react-router";

class NavItem extends React.Component {

	//TO-DO: Update this to be more dynamic.
	navigateToPage() {
		// using let as placeholder and for readablity
		let scrollId = this.props.title;
		let url = this.props.title.replace(/[" "]/g, "-").toLowerCase();
		// hashHistory.isActive(""+url);
 		// console.log(React.findDOMNode(this.refs));

		if(scrollId === "index" || scrollId === "") {
			window.scrollTo(0, 0);
		}
		else if(scrollId === "About Us") {
			window.scrollTo(0, 1000);
		}
		else if(scrollId === "How It Works") {
			window.scrollTo(0, 2000);
		}
		else if(scrollId === "Comments"){
			window.scrollTo(0, 3000);
		}
		console.log(this);
	}	

	render() {		
		return (
				<li class={'nav-item item-'+this.props.id}
					onClick={::this.navigateToPage}>
					{this.props.title.toUpperCase()}
				</li>
		);
	}
}
export default NavItem;