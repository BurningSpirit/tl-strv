import React from 'react';

class ListItem extends React.Component {

	getImage(item) {
		let url = "../src/img/"+item+".png";
		return url;
	}
	componentWillMount() {
		let value = this.props.data;
		let listNumber = this.props.listItemNumber;
		listNumber++;
		this.state = {
				title: value.title,
				image: value.image,
				content: value.content,
				listNumber: listNumber
		}
	}
	handleDifferentItem() {
		if(this.state.image) {
			return <div className="img-holder"><img src={this.getImage(this.state.image)} alt={this.state.image} /></div>
		}
		else {
			console.log('no image for list');
			return <span className="list-number-bg">{this.state.listNumber}</span>
		}
	}
	render() {	
			
		return (
				<li>
				    {this.handleDifferentItem()}
					<div className="content">
						<h3>{this.state.title}</h3>
						<p>{this.state.content}</p>
					</div>
				</li>
		);
	}
}
export default ListItem;
