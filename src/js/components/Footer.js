import React from "react";

import SOCIALMOCKS from '../mocks/SOCIAL.json';

class Footer extends React.Component {

	componentWillMount() {
		// load nav items on mount for data
		let loadData = this.getFooterDetails(); 
	  	if(SOCIALMOCKS && loadData == null) {
	  		this.handleFooterDetails(SOCIALMOCKS);
	  		console.log(this.state);
	  	}

	  	else {
	  		this.handleFooterDetails(loadData)
	  	}
	}
// simple setting the state for navItems Object with data
	handleFooterDetails(data) {
		this.state = { 
			title : data.title,
			socialList : data.social
		};
	}
	getImage(iconUrl){
		let url = "../src/img/icn-"+iconUrl+"@2x.png";
		return url;
	}	
// function to 
// make server call default to null for mocks to take effect
	getFooterDetails() {
		return null;
	}
	//should probably change to another component but wanted to try something different
	renderSocialItem(socialItem, indexValue) {
		return <a class="social-item" href={socialItem.url} index={indexValue}>
					<img src={this.getImage(socialItem.title)} alt={socialItem.title} />
				</a>		
	}

	render() {		
		return (
			<footer>
				{this.state.title.toUpperCase()}
				{this.state.socialList.map((socialItem, i) => {
			        return this.renderSocialItem(socialItem, i);
			    })}
			</footer>
		);
	}
}
export default Footer;