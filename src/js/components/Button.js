import React from 'react';
import Icon from './Button/Icon';
import { Link, hashHistory } from "react-router";

class Button extends React.Component {

	navigateToPage() {
		let url = this.props.url;
		//This is deprecate but will fix later
		hashHistory.pushState(null, url);
		window.scrollTo(0, 3000);
	}

	render() {	
		return (
			<button onClick={::this.navigateToPage}>
				{this.props.title.toUpperCase()}
				<Icon />
			</button>
		);
	}
}
export default Button;