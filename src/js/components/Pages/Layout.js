import React from 'react';

import Header from '../Header';
import Footer from '../Footer';
import Home from '../Pages/Home';
import AboutUs from '../Pages/AboutUs';
import HowItWorks from '../Pages/HowItWorks';
import Comments from '../Pages/Comments';
import BuildYourOwn from '../Pages/BuildYourOwn';


export default class Layout extends React.Component {
	render() {
		//passing props to header so it has access to history needed for header important directly on the module may be better
						// {this.props.children}
		return (
			<div className='wrapper'>
				<Header />

					<Home ref="home"/>
					<AboutUs ref="about-us" />
					<HowItWorks ref="how-it-works"/>
					<Comments ref="comments"/>
					<BuildYourOwn ref="build-your-own" />
					
				<Footer />
			</div>
		);
	}
}
