import React from "react";
import Banner from "../Banner";

import BUILDYOUROWNMOCK from '../../mocks/BUILDYOUROWN.json';

class BuildYourOwn extends React.Component {

	componentWillMount() {
		// load nav items on mount for data
		let loadData = this.getData(); 
	  	if(BUILDYOUROWNMOCK && loadData == null) {
	  		this.state = BUILDYOUROWNMOCK;
	  		console.log(BUILDYOUROWNMOCK);
	  	}

	  	else {
	  		this.state = loadData;
	  	}
	}
	// make server effect
	getData() {
		return null;
	}

	render() {		
		return (
			<div className="build-your-own">
				<Banner data={this.state}/>
			</div>
		);
	}
}
export default BuildYourOwn;