import React from "react";
import PageHead from "../PageHead";
import ReactDOM from 'react-dom';

import COMMENTSMOCKS from '../../mocks/COMMENTS.json';

class Comments extends React.Component {
	constructor() {
		super();
		this.state = {
			data: '',
			counter: ''
		}
	}
	// Should probably seperate into more components but wanted to try and play around with map and react.
	componentWillMount() {
		// load nav items on mount for data // deal with loop for getting the amount of comments
		let loadData = this.getData(); 
	  	if(COMMENTSMOCKS && loadData == null) {
	  		this.state.data = COMMENTSMOCKS;
	  		let replies = 0;
	  		for (let value of COMMENTSMOCKS) {
	  			replies = value.replies.length+replies;
			}
			this.state.counter = replies+COMMENTSMOCKS.length;

	  	}
	  	else {
	  		this.state.data = loadData;
	  	}
	}
	getImage(item){
		let url = "../src/img/"+item+".png";
		return url;
	}
	// make server effect
	getData() {
		return null;
	}

	// this render should probably be split out but I just wanted to play around with react to see it 
	renderComment(commentData, i) {
		let avatar = commentData.details.avatar,
			name = commentData.details.name,
			date = commentData.details.date,
			content = commentData.commentText.content,
			isReply = commentData.replies ? false : true;

		// console.log(avatar,name,date,content);
		return 	<div className={i}>
					<div className="comment-head">				
					<img src={this.getImage(avatar)} alt={avatar} />
						<div className="comment-details">
							<h4>{name}</h4>
							<p>{date}</p>
						</div>
						 {isReply ? false : <i onClick={() => this.addComment(null,commentData)}><img src={this.getImage("reply-icn@2x")} alt={"icon"} /></i>}
					</div>
					<div className="comment-text">{content}</div>
				</div>
	}

	renderContent(commentData, i) {
		let replies = commentData.replies;

	    if (replies.length > 0) {
	    	return <div>
		    		<div className="comment">
			    		{this.renderComment(commentData, i)}
			    	</div>
		           	<div className="replies">
							{replies.map((repliesData, i) => {
						        return this.renderComment(repliesData, i);
						    })}
			        </div>
		        </div>
	    }
	    else {
	    	return <div className="comment">{this.renderComment(commentData, i)}</div>
	    }

	}

	getDate() {
		let today = new Date(),
			dd = today.getDate(),
			mm = today.getMonth()+1; //January is 0!

		let yyyy = today.getFullYear();
		if(dd<10){
		    dd='0'+dd
		} 
		if(mm<10){
		    mm='0'+mm
		} 
		return today = dd+'/'+mm+'/'+yyyy;
	}
	handleChange(event){
	  this.state.value = event.target.value;
	}
	addComment(event, reply) {
		let commentObj = {
			details: {
				avatar: "Avatar@2x",
				date: this.getDate(),
				name: "Guest"	
			},
			commentText: {
				content: this.state.value
			}
		}

		if(this.state.value == null || this.state.value === '' || !this.refs.commentInput.value) {
			ReactDOM.findDOMNode(this.refs.commentInput).focus();
		}
		else if(reply.type !== 'react-click' && reply.replies) {
			commentObj.commentText.content = this.refs.commentInput.value;

			this.state.counter++;
			reply.replies.push(commentObj);
			delete this.state.value;
			this.forceUpdate();
			this.refs.commentInput.value = '';
			ReactDOM.findDOMNode(this.refs.commentInput).focus();
		}
		else {
			// abstract to obj item and instance a new object
			commentObj.replies = [];

			this.state.counter++;
			this.state.data.unshift(commentObj);
			delete this.state.value;
			this.forceUpdate();
			this.refs.commentInput.value = '';
			ReactDOM.findDOMNode(this.refs.commentInput).focus();
		}

	}
	
	renderCommentInput() {
		return <div className="comment-input">
						<img src={this.getImage("Avatar@2x")} alt={"avatar"} />
						<input onChange={::this.handleChange}  type="text" ref="commentInput" value={this.state.value} placeholder="Write your comment here..." />
						<i onClick={() => this.addComment(null,event)}><img className="send" src={this.getImage("icn-send@2x")} alt={"icon"} /></i>
					</div>
    }
	render() {	

		return (
				<div className="comments">
					<PageHead subIndex="03" titleValue="comments" />
					{this.renderCommentInput()}
					
					<h3 className="comment-count">{this.state.counter}&nbsp;Comments</h3>
					<div className="comment-box">
						{this.state.data.map((commentData, i) => {
					        return this.renderContent(commentData, i);
					    })}
					</div>
				</div>
		);
	}
}
export default Comments;