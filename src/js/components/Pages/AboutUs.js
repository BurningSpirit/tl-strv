import React from "react";
import List from "../List";
import PageHead from "../PageHead";

import ABOUTUSMOCK from '../../mocks/ABOUTUS.json';

class AboutUs extends React.Component {

	componentWillMount() {
		// load nav items on mount for data
		let loadData = this.getData(); 
	  	if(ABOUTUSMOCK && loadData == null) {
	  		this.handleData(ABOUTUSMOCK);
	  	}

	  	else {
	  		this.handleData(loadData);
	  	}
	}
	// make server effect
	getData() {
		return null;
	}
	handleData(value) {
		this.state = {
				route: value.route,
				name: value.name,
				list: value.list	
		}
	}
	render() {			
		return (
				<div className={this.state.route}>
					<PageHead subIndex="01" titleValue={this.state.name} />
					<List listType={this.state.route}  list={this.state.list}/>
				</div>
		);
	}
}
export default AboutUs;