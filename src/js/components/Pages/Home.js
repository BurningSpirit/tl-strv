import React from "react";
import Banner from "../Banner";

import HOMEMOCK from '../../mocks/HOME.json';

class Home extends React.Component {

	componentWillMount() {
		// load nav items on mount for data
		let loadData = this.getData(); 
	  	if(HOMEMOCK && loadData == null) {
	  		this.state = HOMEMOCK;
	  		console.log(HOMEMOCK);
	  	}

	  	else {
	  		this.state = loadData;
	  	}
	}
	// make server effect
	getData() {
		return null;
	}
	render() {		
		return (
			<div className="home">
				<Banner data={this.state}/>
			</div>
		);
	}
}
export default Home;