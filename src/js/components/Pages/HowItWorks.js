import React from "react";
import List from "../List";
import Button from "../Button";
import PageHead from "../PageHead";

import HOWITWORKS from '../../mocks/HOWITWORKS.json';


class HowItWorks extends React.Component {
	
	componentWillMount() {
		// TODO: refactor to singleton // find better way
		// load nav items on mount for data
		let loadData = this.getData(); 
	  	if(HOWITWORKS && loadData == null) {
	  		this.handleData(HOWITWORKS);
	  	}

	  	else {
	  		this.handleData(loadData);
	  	}
	}
	// make server effect
	getData() {
		return null;
	}
	handleData(value) {
		this.state = {
				route: value.route,
				name: value.name,
				button: value.button,
				list: value.list	
		}
	}
	render() {	
		return (
				<div className={this.state.route}>
					<PageHead subIndex="02" titleValue={this.state.name} />
					<List listType={this.state.route}  list={this.state.list}/>
					<Button title={this.state.button.title} url={this.state.button.url}/>
				</div>
		);
	}
}
export default HowItWorks;