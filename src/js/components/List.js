import React from 'react';
import ListItem from './List/ListItem';

class List extends React.Component {

	componentWillMount() {
		let list = this.props.list;

		if(list.length > 0) {
				this.state = list;
		}
		else {
			console.log('no list items');
		}
	}
	render() {	
		return (
			<ul className='list' >
				{this.state.map((listItem, i) => {
			        return <ListItem data={listItem} key={i} listItemNumber={i} />;
			    })}
			</ul>
		);
	}
}
export default List;