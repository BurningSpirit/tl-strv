import React from 'react';

class PageHead extends React.Component {

	render() {		
		return (
			<div className="head">
				<h4>{this.props.subIndex} </h4>
				<h3>{this.props.titleValue.toUpperCase()}</h3>
				<div className="red-bar"></div>
			</div>
		);
	}
}
export default PageHead;