import React from "react";
import ReactDOM from "react-dom";
import { Router, Route, IndexRoute, hashHistory } from "react-router";

import AboutUs from "./components/Pages/AboutUs";
import BuildYourOwn from "./components/Pages/BuildYourOwn";
import Comments from "./components/Pages/Comments";
import Home from "./components/Pages/Home";
import HowItWorks from "./components/Pages/HowItWorks";
import Layout from "./components/Pages/Layout";

const strvApp = document.getElementById("app");
// Rendering componets to the 'app' div and initializing routing
ReactDOM.render(
	<Router history={hashHistory}>
		<Route path="/" component={Layout}>
			<IndexRoute component={Home} name="home"></IndexRoute>
			<Route path="about-us" name="about" component={AboutUs}></Route>
			<Route path="how-it-works" name="works" component={HowItWorks}></Route>
			<Route path="comments" name="comments" component={Comments}></Route>
			<Route path="build-your-own" name="build" component={BuildYourOwn}></Route>
		</Route>
	</Router>,
strvApp);